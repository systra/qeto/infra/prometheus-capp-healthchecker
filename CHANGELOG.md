 # Changelog
 All notable changes to this project will be documented in this file.
 
 The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
 and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).
 
## 1.5.1
### Fixed
- Try to keep it running, no matter what happens
- Security updates

## 1.5.0
### Changed
- Migrate from pipenv to poetry.

## 1.4.2
### Fixed
- Fixed security dependencies

## 1.4.0
### Fixed
- Upgrades setuptools and wheel packages
### Added
- CI configuration for publishing, type checking, linting, secrets and security, and tagging

## 1.3.0
### Added
- Publish docker image from gitlab
